use crate::core::RgbBackend;

pub struct RainbowEffect<'a, T> {
    backend: &'a mut T,
    p_led_pos: u16,
}

impl<'a, T> RainbowEffect<'a, T>
where
    T: RgbBackend,
{
    pub fn new(backend: &'a mut T) -> Self {
        Self {
            backend: backend,
            p_led_pos: 0,
        }
    }

    pub fn set_leds(&mut self, size: u16) {
        for i in 0..size {
            self.backend.set(i, 0, 0, 0);
        }
    }

    pub fn animate(&mut self) -> Result<(), ()> {
        if self.p_led_pos != 0 {
            self.backend.set(self.p_led_pos - 1, 0, 0, 0);
        }
        self.backend.set(self.p_led_pos, 255, 255, 255);
        if self.p_led_pos > 480 {
            self.p_led_pos = 0;
        } else {
            self.p_led_pos += 1;
        }
        self.backend.dispatch().unwrap();
        Ok(())
    }
}
