use crate::core::RgbBackend;
use std::convert::TryInto;

pub struct GradientEffect<'a, T> {
    backend: &'a mut T,
    p_led_pos: u16,
}

impl<'a, T> GradientEffect<'a, T>
where
    T: RgbBackend,
{
    pub fn new(backend: &'a mut T) -> Self {
        Self {
            backend: backend,
            p_led_pos: 0,
        }
    }

    pub fn set_leds(&mut self, size: usize) {
        for i in 0u16..(size as u16) {
            self.backend.set(i, 0, 0, 0);
        }
        self.backend.dispatch().unwrap();
    }

    pub fn linterp(&self, a: f32, b: f32, p: f32) -> u8 {
        return (a + p * (b - a)) as u8;
    }

    pub fn generate_gradient(&mut self, indexes: Vec<u8>, size: usize) -> Vec<u8> {
        let mut out_data: Vec<u8> = Vec::with_capacity(size);

        let sf: f32 = (((indexes.len() - 1) as f32) / ((size - 1) as f32)) as f32;
        out_data.push(*indexes.first().unwrap()); // insert first item which is always a constant

        for i in 1..(size-1) {
            let temporary_index = (i as f32) * sf;
            let prev = indexes[(temporary_index.floor()) as usize] as f32;
            let next = indexes[(temporary_index.ceil()) as usize] as f32;
            let point = temporary_index - (temporary_index.floor());

            out_data.push(self.linterp(prev, next, point));
        }

        out_data.push(*indexes.last().unwrap());
        out_data
    }

    pub fn animate(&mut self) -> Result<(), ()> {
        let total_len = 100usize;
        self.set_leds(total_len);

        let r = self.generate_gradient(vec![255, 0, 255, 0, 234], total_len);
        let g = self.generate_gradient(vec![5, 0, 0, 10, 2], total_len);
        let b = self.generate_gradient(vec![20, 10, 10, 20, 10], total_len);

        for i in 0..total_len {
            self.backend.set(i.try_into().unwrap(), r[i], g[i], b[i]);
        }
        
        self.backend.dispatch().unwrap();
        Ok(())
    }
}
