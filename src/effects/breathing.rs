use crate::core::RgbBackend;

pub struct BreathingEffect<'a, T> {
    backend: &'a mut T,
    b_led_pos: u16,
    f_led_pos: u16,
}

impl<'a, T> BreathingEffect<'a, T>
where
    T: RgbBackend,
{
    pub fn new(backend: &'a mut T) -> Self {
        Self {
            backend: backend,
            b_led_pos: 120,
            f_led_pos: 120,
        }
    }

    pub fn set_leds(&mut self, size: u16) {
        for i in 0..size {
            self.backend.set(i, 0, 0, 0);
        }
    }

    pub fn animate(&mut self) -> Result<(), ()> {
        let (r, g, b) = (25u8, 25u8, 112u8);
        self.set_leds(240);
        self.backend.set(self.b_led_pos, r, g, b);
        self.backend.set(self.f_led_pos, r, g, b);

        if self.b_led_pos > 240 {
            self.b_led_pos = 120;
        } else {
            self.b_led_pos +=1;
        }

        if self.f_led_pos == 0 {
            self.f_led_pos = 120;
        } else {
            self.f_led_pos -= 1;
        }

        self.backend.dispatch().unwrap();
        Ok(())
    }
}
