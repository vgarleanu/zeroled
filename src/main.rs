extern crate multizip;

pub mod backends;
pub mod core;
pub mod effects;

use crate::core::RgbBackend;
use backends::ws2812b_server::WS2812B;
use effects::*;
use std::{thread, time};

fn main() {
    let mut backend = WS2812B::new("192.168.0.13", 8333);
    let mut effect = effects::gradient::GradientEffect::new(&mut backend);

    effect.set_leds(100);
    let fps = 60;
    let mut tick = 0;
    /*loop {
        if tick < 60 {
            tick+=1;
        } else {
            tick = 0;
        };
        effect.animate().unwrap();
        thread::sleep(time::Duration::from_millis(1000 / fps));
    }*/

    effect.animate().unwrap();
}
