deps_config := \
	/home/hinach4n/.esp-idf/components/app_trace/Kconfig \
	/home/hinach4n/.esp-idf/components/aws_iot/Kconfig \
	/home/hinach4n/.esp-idf/components/bt/Kconfig \
	/home/hinach4n/.esp-idf/components/driver/Kconfig \
	/home/hinach4n/.esp-idf/components/efuse/Kconfig \
	/home/hinach4n/.esp-idf/components/esp32/Kconfig \
	/home/hinach4n/.esp-idf/components/esp_adc_cal/Kconfig \
	/home/hinach4n/.esp-idf/components/esp_event/Kconfig \
	/home/hinach4n/.esp-idf/components/esp_http_client/Kconfig \
	/home/hinach4n/.esp-idf/components/esp_http_server/Kconfig \
	/home/hinach4n/.esp-idf/components/esp_https_ota/Kconfig \
	/home/hinach4n/.esp-idf/components/espcoredump/Kconfig \
	/home/hinach4n/.esp-idf/components/ethernet/Kconfig \
	/home/hinach4n/.esp-idf/components/fatfs/Kconfig \
	/home/hinach4n/.esp-idf/components/freemodbus/Kconfig \
	/home/hinach4n/.esp-idf/components/freertos/Kconfig \
	/home/hinach4n/.esp-idf/components/heap/Kconfig \
	/home/hinach4n/.esp-idf/components/libsodium/Kconfig \
	/home/hinach4n/.esp-idf/components/log/Kconfig \
	/home/hinach4n/.esp-idf/components/lwip/Kconfig \
	/home/hinach4n/.esp-idf/components/mbedtls/Kconfig \
	/home/hinach4n/.esp-idf/components/mdns/Kconfig \
	/home/hinach4n/.esp-idf/components/mqtt/Kconfig \
	/home/hinach4n/.esp-idf/components/nvs_flash/Kconfig \
	/home/hinach4n/.esp-idf/components/openssl/Kconfig \
	/home/hinach4n/.esp-idf/components/pthread/Kconfig \
	/home/hinach4n/.esp-idf/components/spi_flash/Kconfig \
	/home/hinach4n/.esp-idf/components/spiffs/Kconfig \
	/home/hinach4n/.esp-idf/components/tcpip_adapter/Kconfig \
	/home/hinach4n/.esp-idf/components/unity/Kconfig \
	/home/hinach4n/.esp-idf/components/vfs/Kconfig \
	/home/hinach4n/.esp-idf/components/wear_levelling/Kconfig \
	/home/hinach4n/.esp-idf/components/app_update/Kconfig.projbuild \
	/home/hinach4n/.esp-idf/components/bootloader/Kconfig.projbuild \
	/home/hinach4n/.esp-idf/components/esptool_py/Kconfig.projbuild \
	/home/hinach4n/.esp-idf/components/partition_table/Kconfig.projbuild \
	/home/hinach4n/.esp-idf/Kconfig

include/config/auto.conf: \
	$(deps_config)

ifneq "$(IDF_TARGET)" "esp32"
include/config/auto.conf: FORCE
endif
ifneq "$(IDF_CMAKE)" "n"
include/config/auto.conf: FORCE
endif

$(deps_config): ;
