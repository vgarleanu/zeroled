#include <stdio.h>
#include <string.h>
#include <esp_log.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <nvs_flash.h>
#include <cJSON.h>
#include <esp_system.h>
#include <esp_console.h>
#include <esp_vfs_dev.h>
#include <driver/uart.h>
#include <nvs_flash.h>
#include <linenoise/linenoise.h>
#include <esp_wifi.h>
#include <esp_event_loop.h>
#include <esp_event.h>

#include "led.c"

#define VER 0x0001
#define BOARD "ESP32-WS2812B"

typedef enum {
    STANDARD,
    WIFI,
    ERROR,
} MsgType;


bool send_wifi_ap_msg(char* ssid_c, int32_t rssi_c, char* authmode_c);

esp_err_t event_handler(void *ctx, system_event_t *event){
    if (event->event_id == SYSTEM_EVENT_SCAN_DONE) {
        printf("Number of access points found: %d\n", event->event_info.scan_done.number);
        uint16_t apCount = event->event_info.scan_done.number;
        if (apCount == 0) {
            return ESP_OK;      
        }      
        wifi_ap_record_t *list =        
            (wifi_ap_record_t *)malloc(sizeof(wifi_ap_record_t) * apCount);
        ESP_ERROR_CHECK(esp_wifi_scan_get_ap_records(&apCount, list));
        int i;
        for (i=0; i<apCount; i++) {
            char *authmode;
            switch(list[i].authmode) {
                case WIFI_AUTH_OPEN:
                    authmode = "OPEN";
                    break;
                case WIFI_AUTH_WEP:
                    authmode = "WEP";
                    break;
                case WIFI_AUTH_WPA_PSK:
                    authmode = "WPA_PSK";
                    break;
                case WIFI_AUTH_WPA2_PSK:
                    authmode = "WPA2_PSK";
                    break;
                case WIFI_AUTH_WPA_WPA2_PSK:
                    authmode = "WPA_WPA2_PSK";
                    break;
                default:
                    authmode = "Unknown";
                    break;
            }
            send_wifi_ap_msg((char*)list[i].ssid, list[i].rssi, authmode);
        }
        free(list);
    }
    return ESP_OK;
}

static void initialize_console()
{
    /* Disable buffering on stdin */
    setvbuf(stdin, NULL, _IONBF, 0);

    esp_vfs_dev_uart_set_rx_line_endings(ESP_LINE_ENDINGS_CR);
    /* Move the caret to the beginning of the next line on '\n' */
    esp_vfs_dev_uart_set_tx_line_endings(ESP_LINE_ENDINGS_CRLF);

    /* Configure UART. Note that REF_TICK is used so that the baud rate remains
     * correct while APB frequency is changing in light sleep mode.
     */
    const uart_config_t uart_config = {
        .baud_rate = CONFIG_CONSOLE_UART_BAUDRATE,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .use_ref_tick = true
    };
    uart_param_config(CONFIG_CONSOLE_UART_NUM, &uart_config);

    /* Install UART driver for interrupt-driven reads and writes */
    uart_driver_install(CONFIG_CONSOLE_UART_NUM,
            256, 0, 0, NULL, 0);

    /* Tell VFS to use UART driver */
    esp_vfs_dev_uart_use_driver(CONFIG_CONSOLE_UART_NUM);

    /* Initialize the console */
    esp_console_config_t console_config = {
        .max_cmdline_args = 8,
        .max_cmdline_length = 256,
    };
    esp_console_init(&console_config);

    /* Configure linenoise line completion library */
    /* Enable multiline editing. If not set, long commands will scroll within
     * single line.
     */
    linenoiseSetMultiLine(1);

    /* Tell linenoise where to get command completions and hints */
    linenoiseSetCompletionCallback(&esp_console_get_completion);
    linenoiseSetHintsCallback((linenoiseHintsCallback*) &esp_console_get_hint);

    /* Set command history size */
    linenoiseHistorySetMaxLen(0);
}

bool send_wifi_ap_msg(char* ssid_c, int32_t rssi_c, char* authmode_c) {
    cJSON *data = cJSON_CreateObject();

    cJSON *ssid = cJSON_CreateString(ssid_c);
    cJSON *rssi = cJSON_CreateNumber(rssi_c);
    cJSON *authmode = cJSON_CreateString(authmode_c);

    if(ssid == NULL || rssi == NULL || authmode == NULL || data == NULL) {
        return false;
    }

    cJSON_AddItemToObject(data, "ssid", ssid);
    cJSON_AddItemToObject(data, "rssi", rssi);
    cJSON_AddItemToObject(data, "authmode", authmode);

    cJSON *complete = cJSON_CreateObject();
    cJSON *type = cJSON_CreateString("wifi_ap");

    if(complete == NULL || type == NULL) {
        return false;
    }

    cJSON_AddItemToObject(complete, "type", type);
    cJSON_AddItemToObject(complete, "message", data);

    char *out_string = cJSON_Print(complete);

    if(out_string == NULL) {
        return false;
    }
    ets_printf("%s\n", out_string);
    cJSON_Delete(complete);
    return true;
}

bool send_standard_msg(char* msg){
    cJSON *data = cJSON_CreateObject();

    cJSON *type = cJSON_CreateString("standard");
    cJSON *message = cJSON_CreateString(msg);

    if(type == NULL || message == NULL || data == NULL) {
        return false;
    }

    cJSON_AddItemToObject(data, "type", type);
    cJSON_AddItemToObject(data, "message", message);

    char *out_string = cJSON_Print(data);

    if(out_string == NULL) {
        return false;
    }

    ets_printf("%s\n", out_string);
    cJSON_Delete(data);
    return true;
}

bool jsprintf(char* msg, MsgType msgtype){
    if(msgtype == STANDARD) {
        return send_standard_msg(msg);
    }
    return false;
}

void scan_wifi() {
    // Let us test a WiFi scan ...   
    wifi_scan_config_t scanConf = {
        .ssid = NULL,
        .bssid = NULL,
        .channel = 0,
        .show_hidden = 1
    };
    ESP_ERROR_CHECK(esp_wifi_scan_start(&scanConf, 0));
}

int connect_wifi(char *pch){
    return 0;
}

void set_wifi(char *pch) {
    if(strcmp(pch, "scan") == 0) {
        scan_wifi();
    } else if(strcmp(pch, "connect") == 0) {
        connect_wifi(pch);
    }
}

void prompt(void *pvParams) {
    char *pch;
    while(1) {
        char *line = linenoise(">");
        if(line != NULL) {
            pch = strtok(line, " ");
            if(strcmp(pch, "wifi") == 0) {
                pch = strtok(NULL, " ");
                set_wifi(pch);
            } else if(strcmp(pch, "init") == 0) {
                xTaskCreate(udp_server, "udp_server", 4096, NULL, 5, NULL);
                jsprintf("OK", STANDARD);
            }
        }
    }
}

void app_main() {
    char *msg[64];
    nvs_flash_init();
    tcpip_adapter_init();
    ESP_ERROR_CHECK(esp_event_loop_init(event_handler, NULL));
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_start());
    esp_log_level_set("*", ESP_LOG_ERROR);
    sprintf(msg, "Starting boot delay");
    initialize_console();
    sprintf(msg, "%s_%d", BOARD, VER);
    jsprintf(msg, STANDARD);
    prompt(0);
}
