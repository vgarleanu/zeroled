#include "led.h"
#include <rom/ets_sys.h>
#include <xtensa/hal.h>

#define CYCLE_RES 10

#define T0H_D 400 // 400ns +/- 150ns
#define T1H_D 850 // 850ns +/- 150ns
#define T0L_D 850 // 850ns +/- 150ns
#define T1L_D 400 // 850ns +/- 150ns
#define RES_D 50000 // 50000ns

#define NOP() asm volatile ("nop")

void us_sleep(uint16_t delay) {
    uint32_t startCounter = xthal_get_ccount();
    while(startCounter - xthal_get_ccount() > delay / CYCLE_RES) {
        NOP();
    }
}
