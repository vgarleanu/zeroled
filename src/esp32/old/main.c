#include <esp_log.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <nvs_flash.h>
#include "tcp_server.h"

#define HIGH 1
#define LOW 0
#define OUTPUT GPIO_MODE_OUTPUT
#define INPUT GPIO_MODE_INPUT

void gpioSetup(int gpioNum, int gpioMode, int gpioVal) {
    gpio_num_t gpioNumNative = (gpio_num_t)(gpioNum);
    gpio_mode_t gpioModeNative = (gpio_mode_t)(gpioMode);
    gpio_pad_select_gpio(gpioNumNative);
    gpio_set_direction(gpioNumNative, gpioModeNative);
    gpio_set_level(gpioNumNative, gpioVal);
}

void app_main() {
    ESP_ERROR_CHECK( nvs_flash_init() );
    gpioSetup(13, OUTPUT, LOW);
    ets_printf("Init WiFi\n");
    initialise_wifi();
    wait_for_ip();
    xTaskCreate(udp_server_task, "tcp_server", 4096, NULL, 5, NULL);
}
