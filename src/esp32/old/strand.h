#ifndef STRAND_H
#define STRAND_H
#include <math.h>
#include "esp32_digital_led_lib.h"

#define DEC 10
#define HEX 16
#define OCT 8
#define BIN 2

#define min(a, b)  ((a) < (b) ? (a) : (b))
#define max(a, b)  ((a) > (b) ? (a) : (b))
#define floor(a)   ((int)(a))
#define ceil(a)    ((int)((int)(a) < (a) ? (a+1) : (a)))

#define LEDS 480
#define BYTES_PER_LED 5

extern strand_t STRANDS[];

extern int STRANDCNT;

void gpioSetup(int gpioNum, int gpioMode, int gpioVal);
void delay(uint32_t msg);
void callback(strand_t* pstrand, char* data, size_t size);
void init(void);
#endif
