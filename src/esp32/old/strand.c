#include <freertos/FreeRTOS.h>
#include <driver/gpio.h>
#include <freertos/task.h>
#include "esp_log.h"
#include "strand.h"

strand_t STRANDS[] = {
    {.rmtChannel = 1, .gpioNum = 13, .ledType = LED_WS2812B_V2, .brightLimit = 255, .numPixels =  480,
        .pixels = NULL, ._stateVars = NULL},
}; 

int STRANDCNT = sizeof(STRANDS)/sizeof(STRANDS[0]); 

void delay(uint32_t ms)
{
    if (ms > 0) {
        vTaskDelay(ms / portTICK_PERIOD_MS);
    }
}

void callback(strand_t* pstrand, char* data, size_t size) {
/*    for(uint16_t i = 0; i < 480; i++) {
            pstrand->pixels[i] = pixelFromRGB(0, 0, 0);
    } */
    for(uint16_t i = 0; i < size; i+=5){
        int pixel_i = data[i] | data[i+1] << 8 ;
        ESP_LOGI("STRAND.C", "R: %d, G: %d, B: %d, I: %d", (int)data[i+2], (int)data[i+3], (int)data[i+4], pixel_i);
        if(pixel_i < 480)
            pstrand->pixels[pixel_i] = pixelFromRGB(data[i+2], data[i+3], data[i+4]);
    }
    digitalLeds_updatePixels(pstrand);
}

void init(void){
    if (digitalLeds_initStrands(STRANDS, STRANDCNT)) {
        ESP_LOGI("STRAND.C", "Init FAILURE: halting\n");
        while (true) {};
    }
}
