//! set function trait
//! takes in 4 user arguments which are
//! index for the led and their subsequent colors
//! index can have a max 16bit value while the three colors
//! have a max 255 value.
//! `
//! set(1u16, 255u8, 255u8, 255u8); // to set pixel 1 to white
//! `

pub trait RgbBackend {
    fn set(&mut self, index: u16, r: u8, g: u8, b: u8);
    fn dispatch(&mut self) -> Result<usize, ()>;
}
