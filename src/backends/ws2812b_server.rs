use crate::core;
use std::net::Ipv4Addr;
use std::net::SocketAddr;
use std::net::UdpSocket;
use std::str::FromStr;

#[derive(Clone)]
pub struct PixelData {
    i: u16,
    r: u8,
    g: u8,
    b: u8,
}

pub struct WS2812B {
    led_data: Vec<PixelData>,
    prev_led_data: Vec<PixelData>,
    connection: StripConnection,
}

pub struct StripConnection {
    ip: Ipv4Addr,
    port: u16,
    sock: UdpSocket,
}

impl WS2812B {
    pub fn new(ip: &'static str, port: u16) -> Self {
        let connection = StripConnection::new(ip, port);

        Self {
            led_data: Vec::new(),
            prev_led_data: Vec::new(),
            connection: connection,
        }
    }

    pub fn calc_deltas(&mut self) -> Result<Vec<PixelData>, ()> {
        let deltas = self
            .led_data
            .iter()
            .zip(self.prev_led_data.iter())
            .filter_map(|x| {
                if (x.0.i == x.1.i) & ((x.0.r != x.1.r) | (x.0.g != x.1.g) | (x.0.b != x.1.b)) {
                    Some((*x.0).clone())
                } else if x.0.i != x.1.i {
                    Some((*x.0).clone())
                } else {
                    None
                }
            })
            .collect::<Vec<_>>();
        self.prev_led_data = self.led_data.clone();
        if deltas.len() == 0 {
            return Ok(self.led_data.clone());
        };
        Ok(deltas) // because network is too unrealible, deltas have been removed
    }
}

impl core::RgbBackend for WS2812B {
    fn set(&mut self, index: u16, r: u8, g: u8, b: u8) {
        for x in self.led_data.iter_mut() {
            if x.i == index {
                x.r = r;
                x.g = g;
                x.b = b;
            };
        }

        self.led_data.push(PixelData {
            i: index,
            r: r,
            g: g,
            b: b,
        });
    }

    fn dispatch(&mut self) -> Result<usize, ()> {
        let deltas = self.calc_deltas().unwrap();
        let data = deltas
            .iter()
            .map(|x| {
                let (first, second) = (x.i as u8, (x.i >> 8) as u8);
                [first, second, x.r, x.g, x.b]
            })
            .collect::<Vec<_>>();

        let data: Vec<u8> = data.iter().flat_map(|arr| arr.iter()).cloned().collect();

        match self.connection.send(data) {
            Ok(len) => Ok(len),
            Err(_) => Err(()),
        }
    }
}

impl StripConnection {
    pub fn new(ip: &'static str, port: u16) -> Self {
        Self {
            ip: Ipv4Addr::from_str(ip).unwrap(),
            port: port,
            sock: UdpSocket::bind("0.0.0.0:0").unwrap(),
        }
    }

    // TODO: Split packets up if they reach max MTU size
    pub fn send(&mut self, data: Vec<u8>) -> Result<usize, ()> {
        let length = data.len();
        let size = [(length as u8), (length >> 8) as u8];
        self.sock
            .send_to(&size, SocketAddr::from((self.ip, self.port)))
            .unwrap();
        match self
            .sock
            .send_to(data.as_slice(), SocketAddr::from((self.ip, self.port)))
        {
            Ok(len) => Ok(len),
            Err(_) => Err(()),
        }
    }
}
